from pathlib import Path
import pytest
import keysyms_tk
from keysyms_tk import Deprecation


DEFAULT_KEYSYMS_HEADERS = keysyms_tk.get_system_headers()


@pytest.mark.parametrize("path", DEFAULT_KEYSYMS_HEADERS)
def test_parse_iter(path: Path):
    """Test that we do not alter the original lines"""
    original = path.read_text(encoding="utf-8")
    parsed = "".join(line for line, _ in keysyms_tk.parse_iter(path, None))
    assert original == parsed


@pytest.mark.parametrize("path", DEFAULT_KEYSYMS_HEADERS)
def test_parse_iter_keysyms(path: Path):
    """Test that we do not skip keysyms"""
    keysyms_iter = iter(keysyms_tk.parse_iter_keysyms(path))
    for _, entry in keysyms_tk.parse_iter(path):
        if entry is not None:
            assert entry == next(keysyms_iter)


def is_reference_keysym(
    entry: keysyms_tk.KeysymEntry,
    keysym: int,
    unicode: str | None = None,
    evdev: bool = False,
):
    assert entry.value == keysym
    assert entry.deprecated is Deprecation.NONE
    assert entry.reference_name == entry.name
    assert entry.unicode == unicode
    assert entry.evdev == evdev


def is_deprecated_keysym(
    entry: keysyms_tk.KeysymEntry,
    keysym: int,
    deprecated: Deprecation,
    ref: str,
    unicode: str | None = None,
):
    assert entry.value == keysym
    assert entry.deprecated is deprecated
    if entry.deprecated & Deprecation.IMPLICIT:
        assert entry.reference_name != entry.name
    else:
        assert entry.reference_name == entry.name
    assert entry.reference_name == ref
    assert entry.unicode == unicode


def test_keysyms():
    assert DEFAULT_KEYSYMS_HEADERS[0].name == "keysymdef.h"
    keysyms = keysyms_tk.parse_keysyms(DEFAULT_KEYSYMS_HEADERS[0])

    # keysym = keysyms.get("NoSymbol")
    # assert keysym
    # assert is_reference_keysym(keysym, 0)

    keysym = keysyms.get("VoidSymbol")
    assert keysym
    is_reference_keysym(keysym, 0xFFFFFF)

    keysym = keysyms.get("A")
    assert keysym
    is_reference_keysym(keysym, 0x0041, unicode="A")

    keysym = keysyms.get("braille_dots_123568")
    assert keysym
    is_reference_keysym(keysym, 0x10028B7, unicode="⢷")

    keysym = keysyms.get("Next")
    assert keysym
    is_reference_keysym(keysym, 0xFF56)

    # Implicitly deprecated
    keysym = keysyms.get("Page_Down")
    assert keysym
    is_deprecated_keysym(keysym, 0xFF56, Deprecation.IMPLICIT, "Next")

    # Implicitly deprecated
    keysym = keysyms.get("Page_Down")
    assert keysym
    is_deprecated_keysym(keysym, 0xFF56, Deprecation.IMPLICIT, "Next")

    # Implicitly & explicitly deprecated
    keysym = keysyms.get("quoteright")
    assert keysym
    is_deprecated_keysym(
        keysym,
        0x0027,
        Deprecation.IMPLICIT | Deprecation.EXPLICIT,
        "apostrophe",
        unicode="'",
    )

    # Inexact Unicode mapping
    keysym = keysyms.get("topleftradical")
    assert keysym
    is_deprecated_keysym(
        keysym,
        0x08A2,
        Deprecation.EXPLICIT | Deprecation.INEXACT_UNICODE_MAPPING,
        "topleftradical",
        unicode="┌",
    )


def test_evdev_keysyms():
    assert DEFAULT_KEYSYMS_HEADERS[1].name == "XF86keysym.h"
    keysyms = keysyms_tk.parse_keysyms(DEFAULT_KEYSYMS_HEADERS[1])

    keysym = keysyms.get("XF86MonBrightnessUp")
    assert keysym
    is_reference_keysym(keysym, 0x1008FF02)

    keysym = keysyms.get("XF86BrightnessAuto")
    assert keysym
    is_reference_keysym(keysym, 0x100810F4, evdev=True)


def test_hp_keysyms():
    keysymdef_path = DEFAULT_KEYSYMS_HEADERS[0]
    hpKeysyms_path = DEFAULT_KEYSYMS_HEADERS[4]
    assert keysymdef_path.name == "keysymdef.h"
    assert hpKeysyms_path.name == "HPkeysym.h"
    keysyms = keysyms_tk.parse_keysyms(keysymdef_path, hpKeysyms_path)

    keysym = keysyms.get("hpYdiaeresis")
    assert keysym
    is_reference_keysym(keysym, 0x100000EE)

    # This is from "keysymdef.h"
    keysym = keysyms.get("Ydiaeresis")
    assert keysym
    is_reference_keysym(keysym, 0x13BE, unicode="Ÿ")

    keysym = keysyms.get("hpIO")
    assert keysym
    is_deprecated_keysym(keysym, 0x100000EE, Deprecation.IMPLICIT, "hpYdiaeresis")

    keysym = keysyms.get("IO")
    assert keysym
    is_deprecated_keysym(keysym, 0x100000EE, Deprecation.IMPLICIT, "hpYdiaeresis")
