from __future__ import annotations
import ctypes
import ctypes.util
from typing import TypeAlias

Keysym: TypeAlias = int
KeysymName: TypeAlias = str

xkb_keysym_t = ctypes.c_uint32

class Xkbcommon:
    XKB_KEY_NoSymbol = 0
    XKB_KEYSYM_NO_FLAGS = 0

    def __init__(self, xkbcommon_path: str):
        self._lib = ctypes.cdll.LoadLibrary(xkbcommon_path)

        self._lib.xkb_keysym_from_name.argtypes = [ctypes.c_char_p, ctypes.c_int]
        self._lib.xkb_keysym_from_name.restype = xkb_keysym_t

        self._lib.xkb_keysym_to_utf32.argtypes = [xkb_keysym_t]
        self._lib.xkb_keysym_to_utf32.restype = ctypes.c_uint32

        self._lib.xkb_utf32_to_keysym.argtypes = [ctypes.c_uint32]
        self._lib.xkb_utf32_to_keysym.restype = xkb_keysym_t

        self._lib.xkb_keysym_get_name.argtypes = [
            xkb_keysym_t,
            ctypes.c_char_p,
            ctypes.c_size_t,
        ]
        self._lib.xkb_keysym_get_name.restype = int

    @classmethod
    def load(cls) -> Xkbcommon | None:
        """Try to load xkbcommon"""
        if xkbcommon_path := ctypes.util.find_library("xkbcommon"):
            return cls(xkbcommon_path)
        else:
            return None

    def keysym_from_name(self, keysym_name: KeysymName) -> Keysym:
        return self._lib.xkb_keysym_from_name(
            keysym_name.encode("utf-8"), self.XKB_KEYSYM_NO_FLAGS
        )

    def is_invalid_keysym(self, keysym: Keysym) -> bool:
        return keysym == self.XKB_KEY_NoSymbol

    def is_invalid_keysym_name(self, name: str) -> bool:
        return self.is_invalid_keysym(self.keysym_from_name(name))

    def keysym_to_char(self, keysym: Keysym) -> str | None:
        if self.is_invalid_keysym(keysym):
            raise ValueError(f"Unsupported keysym: “0x{keysym:0>4x}”")
        codepoint = self._lib.xkb_keysym_to_utf32(keysym)
        return chr(codepoint) if codepoint else None

    def keysym_name_to_char(self, keysym_name: KeysymName) -> str | None:
        keysym = self.keysym_from_name(keysym_name)
        if self.is_invalid_keysym(keysym):
            raise ValueError(f"Unsupported keysym: “{keysym_name}”")
        return self.keysym_to_char(keysym)

    def keysym_get_name(self, keysym: Keysym) -> KeysymName:
        buf_len = 90
        buf = ctypes.create_string_buffer(buf_len)
        n = self._lib.xkb_keysym_get_name(keysym, buf, ctypes.c_size_t(buf_len))
        if n < 0:
            raise ValueError(f"Unsupported keysym: 0x{keysym:0>4X})")
        elif n >= buf_len:
            raise ValueError(f"Buffer is not big enough: expected at least {n}.")
        else:
            return buf.value.decode("utf-8")

    def char_to_keysym_name(self, char: str) -> KeysymName:
        keysym = self._lib.xkb_utf32_to_keysym(ord(char))
        if self.is_invalid_keysym(keysym):
            return ""
        else:
            return self.keysym_get_name(keysym)
