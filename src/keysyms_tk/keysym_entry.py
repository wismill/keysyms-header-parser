from __future__ import annotations

import logging
import re
import unicodedata
from dataclasses import dataclass
from enum import Enum, IntFlag, auto, unique
from typing import Any, Generator, TypeAlias

from .unicode import (
    GeneralCategory,
    NameAliases,
    char_alternate_names,
    char_name_or_unassigned,
)
from .xkbcommon import Xkbcommon


@unique
class Deprecation(IntFlag):
    NONE = 0
    EXPLICIT = 1 << 0
    IMPLICIT = 1 << 1
    INEXACT_UNICODE_MAPPING = 1 << 2


Keysym: TypeAlias = int
KeysymName: TypeAlias = str

EVDEV_OFFSET = 0x10081000

@unique
class UnicodeSemantics(Enum):
    NONE = "none"
    GENERIC = "generic"
    KEYPAD = "keypad"

@dataclass
class KeysymEntry:
    value: Keysym
    long_value: bool
    name: KeysymName
    reference_name: KeysymName
    alias: bool
    deprecated: Deprecation
    unicode: str | None
    unicode_name: str | None
    unicode_alternate_names: tuple[str, ...]
    unicode_semantics: UnicodeSemantics
    evdev: bool
    comment: str | None
    c_constant: str


@dataclass
class KeysymRegistry:
    keysyms: dict[Keysym, KeysymEntry]
    values: dict[KeysymName, Keysym]


################################################################################
# Utils
################################################################################


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# def tabs_padding(tab_size: int, target: int, *strings: str) -> int:
#     assert len(strings) > 1
#     current = sum(len(s) for s in strings[:-1])
#     assert target > current
#     offset = current % tab_size
#     return (target - current + offset - len(strings[-1]) + 1) // tab_size


################################################################################
# xkbcommon handling
################################################################################


libxkbcommon = Xkbcommon.load()


################################################################################
# Keysym entry parsing
################################################################################


@dataclass
class KeysymParseResult:
    ok: bool
    entry: KeysymEntry | None


class KeysymEntryParser:
    # Pattern for a keysym entry: #define XK_a 0x0061 /* U+0061 LATIN SMALL LETTER A */
    KEYSYM_ENTRY_PATTERN = re.compile(
        r"""
        ^\#define\s+
        (?:(?P<prefix>\w+)?XK|(?P<xkbcommon>XKB_KEY))_(?P<name>\w+)\s+
        (?P<evdev>_EVDEVK\()?
        (?P<value>0x[0-9a-fA-F]+)
        (?(evdev)\)|)\s*
        (?:/\*(?P<comment>.+)\*/)?
        \s*$
        """,
        re.VERBOSE,
    )
    # Pattern for keysym entry comment: deprecated, Unicode, explicit alias
    KEYSYM_COMMENT_PATTERN = re.compile(
        r"""
        # Skip leading spaces
        \s*
        (?:
        # Explicitly deprecated
        (?P<deprecated>deprecated)|
        # Unicode mapping
        (?:(?P<inexact_unicode>\()|(?P<unicode_special><))?U\+
        (?P<unicode>[0-9a-fA-F]{4,})(?P<unicode_name>(?:\s|\w|-)+)
        (?(inexact_unicode)\))(?(unicode_special)>)|
        # Explicit alias: do not deprecate
        alias\s+for\s+(?P<alias>\w+)
        )
        """,
        re.VERBOSE | re.IGNORECASE,
    )
    # Pattern for parsing comment of evdev keysym entries: kernel version, key
    KEYSYM_EVDEV_COMMENT_PATTERN = re.compile(
        r"""
        (?:(?P<kernel_version>v\d+(?:\.\d+)+)\s+)?
        (?P<key>\w+)
        """,
        re.VERBOSE | re.IGNORECASE,
    )
    # Pattern to detect tokens that are not interpreted in comment
    KEYSYM_COMMENT_TOKENS_PATTERN = re.compile(
        r"""
        deprecated|
        alias|
        \(?\s*U[0-9a-fA-F]{3,}[^)]*\)?
        """,
        re.VERBOSE,
    )
    # Pattern to detect keysym entry guard
    KEYSYM_IFNDEF_PATTERN = re.compile(r"^#ifndef\s+XK_(?P<name>\w+)\s*$")
    # Pattern to detect special commented EVDEV entries
    KEYSYM_COMMENTED_EVDEV_ENTRY_PATTERN = re.compile(
        r"""
        ^/\*\s+Use:\s+
        (?P<name>\w*XK_\w+)\s+
        _EVDEVK\((?P<value>0x[0-9A-Fa-f]+)\)\s+
        (?P<comment>[^\*]+)
        \*/
        """,
        re.VERBOSE | re.IGNORECASE,
    )

    # Keysym with special Unicode conversion
    TTY_RANGE = range(0xFF08, 0xFF1B + 1)
    XK_KP_Space = 0xFF80
    KEYPAD_RANGE = range(0xFF89, 0xFFBD + 1)
    XK_DELETE = 0xFFFF

    def __init__(
        self,
        name_aliases: NameAliases | None,
        registry: KeysymRegistry,
        pending_ifndef: KeysymName | None,
        match: re.Match[str],
    ):
        self.name_aliases: NameAliases = name_aliases or {}
        self.registry = registry
        self.pending_ifndef = pending_ifndef
        self.match = match

        self.keysym: Keysym = 0
        self.long_value: bool = False
        self.name: str = ""
        self.reference_name: str = ""
        self.c_constant: str = ""
        self.deprecation: Deprecation = Deprecation.NONE
        self.alias_target: str | None = None
        self.unicode: str | None = None
        self.unicode_name: str | None = None
        self.unicode_alternate_names: tuple[str, ...] = ()
        self.unicode_special: bool = False
        self.evdev: bool = False
        self.comment: str | None = None

    @classmethod
    def parse_keysym_entry(
        cls,
        name_aliases: NameAliases | None,
        registry: KeysymRegistry,
        pending_ifndef: KeysymName | None,
        line: str,
    ) -> KeysymParseResult:
        """
        Parse a keysym entry: #define XK_a 0x0061 /* U+0061 LATIN SMALL LETTER A */
        """
        if m := cls.KEYSYM_ENTRY_PATTERN.match(line):
            parser = cls(name_aliases, registry, pending_ifndef, m)
            if keysym := parser._handle_keysym_match():
                return KeysymParseResult(True, keysym)
            else:
                return KeysymParseResult(True, None)
        else:
            return KeysymParseResult(False, None)

    def _handle_keysym_match(self):
        self._parse_keysym_value()
        self._parse_name()
        self._parse_c_constant()
        self._parse_comment()

        if self.pending_ifndef and self.pending_ifndef == self.name:
            # Guarded duplicate: skip
            logger.debug(f"Guarded keysym: {self.name}")
            return None
        elif ref_value := self.registry.values.get(self.name):
            # Duplicate keysym: skip
            if ref_value != self.name:
                logger.error(
                    f"Keysym “{self.name}” 0x{self.keysym:0>4x} already defined with another value: 0x{ref_value:0>4x}; skipping."
                )
            return None
        elif ref := self.registry.keysyms.get(self.keysym):
            # Alias: explicit or implicit
            if self.alias_target:
                # Explicit alias: check it is valid
                if ref.name != self.alias_target:
                    if target_value := self.registry.values.get(
                        self.alias_target or ""
                    ):
                        logger.error(
                            f"Keysym “{self.name}” (0x{self.keysym:0>4x}) is declared as alias of “{self.alias_target}” (0x{target_value}), but they have different values"
                        )
                    else:
                        logger.error(
                            f"Keysym “{self.name}” (0x{self.keysym:0>4x}) is declared as alias of “{self.alias_target}”, but the alias does not exists. Typo?"
                        )
                if self.alias_target and not self.deprecated:
                    # Explicit non-deprecated alias
                    self.reference_name = self.name
                else:
                    # Deprecated, because there is a previous definition with other name.
                    self.deprecated |= Deprecation.IMPLICIT
                    self.reference_name = ref.name
            else:
                # Implicit alias
                # Deprecated, because there is a previous definition with other name.
                self.deprecated |= Deprecation.IMPLICIT
                self.reference_name = ref.name
            # Inherit properties from target:
            self.deprecated |= ref.deprecated
            if ref.unicode != self.unicode:
                if not self.unicode:
                    self.unicode = ref.unicode
                    self.unicode_name = ref.unicode_name
                    self.unicode_alternate_names = ref.unicode_alternate_names
                    self.unicode_special = self.unicode_special
                elif not ref.unicode:
                    logger.warning(
                        f"Keysym “{self.name}” maps to U+{ord(self.unicode):0>4X}, but its target “{ref.name}” has not Unicode mapping."
                    )
                else:
                    logger.error(
                        f"Keysym “{self.name}” maps to U+{ord(self.unicode):0>4X}, but its target “{ref.name}” maps to U+{ord(ref.unicode):0>4X}."
                    )
        else:
            # Reference keysym
            if self.alias_target:
                logger.error(
                    f"Explicit alias “{self.name}” for invalid keysym “{self.alias_target}”."
                )
            self.reference_name = self.name

        self._xkbcommon_check()

        if self.unicode:
            if self.unicode_special:
                if self.name.startswith("KP_"):
                    unicode_semantics = UnicodeSemantics.KEYPAD
                else:
                    raise ValueError(f"Unsupported specific Unicode semantics: {self.name}")
            else:
                unicode_semantics = UnicodeSemantics.GENERIC
        else:
            unicode_semantics = UnicodeSemantics.NONE

        entry = KeysymEntry(
            value=self.keysym,
            long_value=self.long_value,
            name=self.name,
            reference_name=self.reference_name,
            alias=bool(self.alias_target)
            or bool(self.deprecated & Deprecation.IMPLICIT),
            deprecated=self.deprecated,
            unicode=self.unicode,
            unicode_name=self.unicode_name,
            unicode_alternate_names=self.unicode_alternate_names,
            unicode_semantics=unicode_semantics,
            evdev=self.evdev,
            comment=self.comment,
            c_constant=self.c_constant,
        )

        self.registry.values[self.name] = self.keysym
        if not ref:
            # Register canonical keysym
            self.registry.keysyms[self.keysym] = entry

        return entry

    def _parse_keysym_value(self):
        if self.match.group("evdev"):
            # _EVDEVK macro
            self.keysym = EVDEV_OFFSET + int(self.match.group("value"), 16)
            self.evdev = True
            self.long_value = False
        else:
            self.keysym = int(self.match.group("value"), 16)
            self.evdev = False
            self.long_value = len(self.match.group("value")) > 9

    def _parse_name(self):
        self.name = (self.match.group("prefix") or "") + self.match.group("name")

    def _parse_c_constant(self):
        if self.match.group("xkbcommon"):
            self.c_constant = f"XKB_KEY_{self.name}"
        else:
            self.c_constant = (
                f"{self.match.group('prefix') or ''}XK_{self.match.group('name')}"
            )

    def _parse_comment(self):
        if comment := self.match.group("comment"):
            self.comment = comment
            if self.evdev:
                if evdev_match := self.KEYSYM_EVDEV_COMMENT_PATTERN.search(comment):
                    self.comment = KeysymEntrySerializer.make_evdev_comment(evdev_match)
                else:
                    logger.warning(
                        f"Keysym “{self.name}” is an evdev keysym, but its comment is not valid."
                    )
                self.deprecated = Deprecation.NONE
            elif comment_match := self.KEYSYM_COMMENT_PATTERN.match(comment):
                if comment_match.group("deprecated"):
                    # Explicitly deprecated
                    self.deprecated = Deprecation.EXPLICIT
                    # Check for alias
                    if comment_matchʹ := self.KEYSYM_COMMENT_PATTERN.search(
                        comment, pos=comment_match.end()
                    ):
                        self.alias_target = comment_matchʹ.group("alias")
                elif comment_match.group("inexact_unicode"):
                    # Explicitly deprecated: inexact Unicode mapping
                    self.deprecated = (
                        Deprecation.EXPLICIT | Deprecation.INEXACT_UNICODE_MAPPING
                    )
                elif alias_target := comment_match.group("alias"):
                    # Explicit alias: do not deprecate
                    self.deprecated = Deprecation.NONE
                    self.alias_target = alias_target
                else:
                    # Normal comment
                    self.deprecated = Deprecation.NONE
                # Check Unicode
                if raw_unicode := comment_match.group("unicode"):
                    self.unicode = chr(int(raw_unicode, 16))
                    self.unicode_name = comment_match.group("unicode_name").strip()
                    assert self.unicode_name
                    self.unicode_special = bool(comment_match.group("unicode_special"))
                    self.comment = None  # Do not keep the comment
                    # Check normalization
                    normalized = unicodedata.normalize("NFC", self.unicode)
                    if self.unicode != normalized and not self.deprecated:
                        logger.warning(
                            f"Keysym “{self.name}” maps to U+{ord(self.unicode)} “{self.unicode}”, but the normal form NFC is U+{ord(normalized)} “{normalized}”."
                        )
                    # Check name
                    unicode_name_ref, gen_cat = char_name_or_unassigned(
                        self.name_aliases, self.unicode
                    )
                    if not self.unicode_name.startswith(unicode_name_ref):
                        logger.warning(
                            f"Keysym “{self.name}” maps to U+{ord(self.unicode)} “{self.unicode_name}”, but the correct name is “{unicode_name_ref}”."
                        )
                        self.unicode_name = unicode_name_ref
                        self.unicode_alternate_names = char_alternate_names(
                            self.name_aliases, self.unicode
                        )
                    elif gen_cat is not GeneralCategory.MISC and not (
                        self.deprecated & Deprecation.INEXACT_UNICODE_MAPPING
                    ):
                        logger.warning(
                            f"Keysym “{self.name}” maps to non standard U+{ord(self.unicode)} codepoint and is not deprecated."
                        )
                    else:
                        self.unicode_alternate_names = char_alternate_names(
                            self.name_aliases, self.unicode
                        )
            elif keywords_match := self.KEYSYM_COMMENT_TOKENS_PATTERN.findall(comment):
                # Further comment check
                logger.warning(
                    f"Possibly incorrect comment: did not catch {keywords_match}"
                )
                self.deprecated = Deprecation.NONE
            else:
                self.deprecated = Deprecation.NONE
        else:
            self.deprecated = Deprecation.NONE

    def _xkbcommon_check(self):
        if libxkbcommon and not libxkbcommon.is_invalid_keysym(self.keysym):
            # Check Unicode
            if unicode_ref := libxkbcommon.keysym_to_char(self.keysym):
                if self.keysym == self.XK_KP_Space:
                    # Special case
                    self.unicode = " "
                    assert unicode_ref == self.unicode
                elif (
                    self.keysym in self.TTY_RANGE
                    or self.keysym in self.KEYPAD_RANGE
                    or self.keysym == self.XK_DELETE
                ):
                    # Special cases
                    self.unicode = chr(self.keysym & 0x7F)
                    assert unicode_ref == self.unicode
                elif not self.unicode:
                    logger.warning(
                        f"Keysym “{self.name}” maps to U+{ord(unicode_ref):0>4X} in xkbcommon, but has no Unicode comment and is not an alias."
                    )
                    self.unicode = unicode_ref
                elif self.unicode != unicode_ref and not self.deprecated:
                    logger.error(
                        f"Keysym “{self.name}” maps to U+{ord(self.unicode):0>4X}, but xkbcommon maps to U+{ord(unicode_ref):0>4X}."
                    )
                if not self.deprecated and not self.alias_target:
                    self.comment = None  # Replace comment
                if not self.unicode_name:
                    self.unicode_name, _ = char_name_or_unassigned(
                        self.name_aliases, self.unicode
                    )
                    self.unicode_alternate_names = char_alternate_names(
                        self.name_aliases, self.unicode
                    )
            elif self.unicode:
                logger.warning(
                    f"Keysym “{self.name}” maps to U+{ord(self.unicode):0>4X}, but xkbcommon does not."
                )

    @classmethod
    def parse_guarded_keysym_entry(cls, line: str) -> str | None:
        if m := cls.KEYSYM_IFNDEF_PATTERN.match(line):
            # Guard keysym
            return m.group("name")
        else:
            return None

    @classmethod
    def parse_commented_evdev_entry(cls, line: str) -> KeysymEntry | None:
        if m := cls.KEYSYM_COMMENTED_EVDEV_ENTRY_PATTERN.match(line):
            return KeysymEntry(
                value=EVDEV_OFFSET + int(m.group("value"), 16),
                long_value=False,
                name=m.group("name"),
                reference_name=m.group("name"),
                alias=True,
                c_constant=m.group("name"),
                deprecated=Deprecation.EXPLICIT,
                evdev=True,
                unicode=None,
                unicode_alternate_names=(),
                unicode_name=None,
                unicode_semantics=UnicodeSemantics.NONE,
                comment=m.group("comment"),
            )
        else:
            return None


######################################################################
# Serialization
######################################################################


class KeysymEntrySerializer:
    COMMENT_SEP = "  "
    TARGET_KEYSYM_COLUMN = 47
    """Column in the file we want the keysym codes to end"""
    TARGET_EVDEV_KEYSYM_COLUMN = 54
    KERNEL_VERSION_PADDING = 7

    def __init__(self):
        ...

    @classmethod
    def make_evdev_comment(cls, m: re.Match[str]):
        return f""" {m.group("kernel_version") or "": <{cls.KERNEL_VERSION_PADDING}} {m.group("key")} """

    def serialize_keysym_entry(self, entry: KeysymEntry) -> str:
        if entry.evdev:
            keysym = f"_EVDEVK(0x{entry.value - EVDEV_OFFSET:0>3x})"
        else:
            padding = 8 if entry.long_value else 4
            keysym = f"0x{entry.value:0>{padding}x}"
        # TODO: implicit → explicit deprecation
        if entry.comment:
            comment = f"{self.COMMENT_SEP}/*{entry.comment}*/"
        elif entry.unicode and not entry.alias:
            if entry.unicode_alternate_names:
                alternate_names = f" ({', '.join(entry.unicode_alternate_names)})"
            else:
                alternate_names = ""
            comment = (
                f"U+{ord(entry.unicode):0>4X} {entry.unicode_name}{alternate_names}"
            )
            if entry.deprecated & Deprecation.INEXACT_UNICODE_MAPPING:
                comment = f"{self.COMMENT_SEP}/*({comment})*/"
            elif entry.unicode_semantics not in (UnicodeSemantics.NONE, UnicodeSemantics.GENERIC):
                comment = f"{self.COMMENT_SEP}/*<{comment}>*/"
            else:
                assert not entry.deprecated
                comment = f"{self.COMMENT_SEP}/* {comment} */"
        elif entry.deprecated:
            # Mark deprecated keysym explicitly
            if entry.deprecated is Deprecation.IMPLICIT:
                comment = f"{self.COMMENT_SEP}/* deprecated alias for {entry.reference_name} */"
            else:
                comment = f"{self.COMMENT_SEP}/* deprecated */"
        else:
            comment = ""

        keysym_padding = (
            (
                self.TARGET_EVDEV_KEYSYM_COLUMN
                if entry.evdev
                else self.TARGET_KEYSYM_COLUMN
            )
            - len("#define ")
            - len(entry.c_constant)
        )
        if keysym_padding <= len(keysym):
            raise ValueError(f"Keysym “{entry.name}”: Insufficient padding")
        else:
            return f"#define {entry.c_constant}{keysym: >{keysym_padding}}{comment}"

    def serialize_commented_keysym_entry(self, entry: KeysymEntry) -> str:
        keysym = f"_EVDEVK(0x{entry.value - EVDEV_OFFSET:0>3x})"

        keysym_padding = (
            self.TARGET_EVDEV_KEYSYM_COLUMN - len("/* Use: ") - len(entry.c_constant)
        )
        if keysym_padding <= len(keysym):
            raise ValueError(f"Insufficient padding for evdev keysym “{entry.name}”")

        assert entry.comment
        if m := KeysymEntryParser.KEYSYM_EVDEV_COMMENT_PATTERN.match(entry.comment):
            comment = self.make_evdev_comment(m)
        else:
            raise ValueError(
                f"Keysym “{entry.name}”: Invalid commented evdev comment: “{entry.comment}”"
            )

        return f"/* Use: {entry.c_constant}{keysym: >{keysym_padding}}    {comment}*/"


class YamlKeysymEntrySerializer:
    def __init__(self):
        ...

    def serialize_iter(self, entry: KeysymEntry) -> Generator[str, Any, None]:
        yield f"{entry.name}: {{ value: 0x{entry.value:0>4x}"
        if entry.name != entry.reference_name:
            yield f', reference: "{entry.reference_name}"'
        if entry.alias:
            yield f", alias: True"
        if entry.deprecated:
            yield f", deprecated: True"
        if entry.unicode:
            yield f', unicode: "{self.escape_string(entry.unicode)}"'
        if entry.unicode_semantics not in (UnicodeSemantics.NONE, UnicodeSemantics.GENERIC):
            yield f", unicode_semantics: {entry.unicode_semantics.value}"
        yield " }"

    def serialize(self, entry: KeysymEntry) -> str:
        return "".join(self.serialize_iter(entry))

    @staticmethod
    def escape_char(c: str) -> str:
        if c == "\\" or c == '"':
            return f"\\{c}"
        if c.isprintable() and not unicodedata.combining(c):
            return c
        elif c <= "\xff":
            return f"\\x{ord(c):0>2x}"
        elif c <= "\uffff":
            return f"\\u{ord(c):0>4x}"
        else:
            return f"\\U{ord(c):0>8x}"

    @staticmethod
    def escape_string(s: str) -> str:
        return "".join(map(YamlKeysymEntrySerializer.escape_char, s))
