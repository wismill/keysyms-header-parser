import sys

MINIMUM_PYTHON_VERSION = (3, 10)
if sys.version_info < MINIMUM_PYTHON_VERSION:
    raise Exception(
        "Minimal Python version required: " + ".".join(map(str, MINIMUM_PYTHON_VERSION))
    )

from .keysym_entry import Deprecation, Keysym, KeysymEntry, KeysymName, KeysymRegistry
from .keysym_header import (
    get_system_headers,
    parse_iter,
    parse_iter_keysyms,
    parse_keysyms,
)
from .unicode import NameAliases, parse_unicode_name_aliases
