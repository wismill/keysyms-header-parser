from collections import defaultdict
from enum import Enum, unique
from pathlib import Path
from typing import TypeAlias
import logging
import unicodedata


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


@unique
class NameAliasType(Enum):
    CORRECTION = "correction"
    CONTROL = "control"
    ALTERNATE = "alternate"
    FIGMENT = "figment"
    # NOTE: unsupported type: abbreviation


NameAliases: TypeAlias = dict[str, dict[NameAliasType, list[str]]]

# Unicode names for some control characters, to avoid raising an error
# if name aliases are not provided.
CONTROL_NAMES = {
    "\u0008": "BACKSPACE",
    "\u0009": "CHARACTER TABULATION",
    "\u000A": "LINE FEED",
    "\u000B": "LINE TABULATION",
    "\u000D": "CARRIAGE RETURN",
    "\u001B": "ESCAPE",
    "\u007F": "DELETE",
}


# We want to use Unicode *corrected/control* names, but the Python
# API does not propose those. So we process the UCD by ourselves.
def parse_unicode_name_aliases(path: Path) -> NameAliases:
    aliases: NameAliases = defaultdict(lambda: defaultdict(list))
    logger.info(f"Processing Unicode name aliases file: {path}")
    with path.open("rt", encoding="utf-8") as fd:
        for line in map(lambda s: s.strip(), fd):
            # Empty line or comment
            if not line or line.startswith("#"):
                continue
            line = line.split("#")[0]
            raw_codepoint, alias, category, *_ = map(
                lambda s: s.strip(), line.split(";")
            )
            char = chr(int(raw_codepoint, 16))
            for alias_type in NameAliasType:
                if category == alias_type.value:
                    aliases[char][alias_type].append(alias)
                    break
    logger.info(f"{path} processed.")
    return aliases


def char_name(name_aliases: NameAliases, c: str) -> str:
    if aliases := name_aliases.get(c):
        names = (
            aliases.get(NameAliasType.CORRECTION)
            or aliases.get(NameAliasType.CONTROL)
            or aliases.get(NameAliasType.FIGMENT)
        )
        name = names[0] if names else None
    elif not (name := unicodedata.name(c, None)):
        name = CONTROL_NAMES.get(c)
    if name is None:
        raise ValueError(f"Cannot find Unicode name for: “{c}” (U+{ord(c):0>4X})")
    else:
        return name


def char_alternate_names(name_aliases: NameAliases, c: str) -> tuple[str, ...]:
    if aliases := name_aliases.get(c):
        return tuple(aliases.get(NameAliasType.ALTERNATE, ()))
    else:
        return ()


UNASSIGNED_UNICODE_CODEPOINT_LABEL = "Unassigned code point"


@unique
class GeneralCategory(Enum):
    """Unicode general category (simplified)"""

    UNASSIGNED_CODEPOINT = "Cn"
    MISC = "misc"


def char_name_or_unassigned(
    name_aliases: NameAliases, c: str
) -> tuple[str, GeneralCategory]:
    try:
        return char_name(name_aliases or {}, c), GeneralCategory.MISC
    except ValueError as err:
        if unicodedata.category(c) == GeneralCategory.UNASSIGNED_CODEPOINT.value:
            return (
                UNASSIGNED_UNICODE_CODEPOINT_LABEL,
                GeneralCategory.UNASSIGNED_CODEPOINT,
            )
        else:
            raise err
