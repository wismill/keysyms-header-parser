#!/usr/bin/env python3

import argparse
import logging
import unicodedata
from pathlib import Path

import keysyms_tk
from keysyms_tk import keysym_entry, Deprecation

logging.basicConfig(format="[{levelname}] {message}", style="{")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def pretty_deprecation(deprecation: Deprecation) -> str:
    # NOTE: d.name may be None
    return ", ".join(d.name or str(d.value) for d in Deprecation if d & deprecation)


KEYSYM_VALUE_PADDING = 8
KEYSYM_NAME_PADDING = 27
UNICODE_PADDING = 11


def load_name_aliases(args: argparse.Namespace) -> keysyms_tk.NameAliases:
    return (
        keysyms_tk.parse_unicode_name_aliases(args.unicode_name_aliases)
        if args.unicode_name_aliases
        else {}
    )


def print_keysyms(args: argparse.Namespace):
    name_aliases = load_name_aliases(args)
    keysyms = keysyms_tk.KeysymRegistry({}, {})
    for path in args.input:
        logger.info(f"Processing file: {path}")
        for entry in keysyms_tk.parse_iter_keysyms(
            path, keysyms, name_aliases=name_aliases
        ):
            if not args.deprecated or entry.deprecated:
                ref = (
                    f" → {entry.reference_name: <{KEYSYM_NAME_PADDING}}"
                    if entry.reference_name != entry.name
                    else " " * (KEYSYM_NAME_PADDING + 3)
                )
                deprecated = pretty_deprecation(entry.deprecated)
                if entry.unicode:
                    char = (
                        f" “{entry.unicode}”"
                        if entry.unicode.isprintable()
                        and not unicodedata.combining(entry.unicode)
                        else ""
                    )
                    unicode = f"U+{ord(entry.unicode):0>4X}{char}"
                else:
                    unicode = ""
                print(
                    f"0x{entry.value:0>{KEYSYM_VALUE_PADDING}x}: {entry.name: >{KEYSYM_NAME_PADDING}}{ref} {unicode: <{UNICODE_PADDING}} ({deprecated})"
                )


def print_header_file(args: argparse.Namespace):
    name_aliases = load_name_aliases(args)
    keysyms = keysyms_tk.KeysymRegistry({}, {})
    serializer = keysym_entry.KeysymEntrySerializer()
    for path in args.input:
        for line, entry in keysyms_tk.parse_iter(
            path, keysyms, name_aliases=name_aliases
        ):
            if entry:
                print(serializer.serialize_keysym_entry(entry))
            elif entry := keysym_entry.KeysymEntryParser.parse_commented_evdev_entry(
                line
            ):
                print(serializer.serialize_commented_keysym_entry(entry))
            else:
                print(line, end="")


def print_yaml_file(args: argparse.Namespace):
    name_aliases = load_name_aliases(args)
    keysyms = keysyms_tk.KeysymRegistry({}, {})
    serializer = keysym_entry.YamlKeysymEntrySerializer()
    for path in args.input:
        for entry in keysyms_tk.parse_iter_keysyms(
            path, keysyms, name_aliases=name_aliases
        ):
            print(serializer.serialize(entry))


def add_common_args(parser: argparse.ArgumentParser):
    parser.add_argument(
        "input",
        type=Path,
        nargs="*",
        default=keysyms_tk.get_system_headers(),
    )
    parser.add_argument(
        "--unicode-name-aliases",
        type=Path,
        help="Name aliases file from the Unicode Character Database. Latest version available at: https://www.unicode.org/Public/UCD/latest/ucd/NameAliases.txt",
    )


def main():
    parser = argparse.ArgumentParser(description="Keysyms header parser")
    subparser = parser.add_subparsers()

    keysyms_parser = subparser.add_parser("keysyms", help="Print keysyms")
    keysyms_parser.set_defaults(func=print_keysyms)
    add_common_args(keysyms_parser)
    keysyms_parser.add_argument(
        "-d", "--deprecated", action="store_true", help="Show only deprecated keysyms"
    )

    print_yaml = subparser.add_parser("yaml", help="Serialize keysyms to Yaml")
    print_yaml.set_defaults(func=print_yaml_file)
    add_common_args(print_yaml)

    print_parser = subparser.add_parser("print", help="Print header files")
    print_parser.set_defaults(func=print_header_file)
    add_common_args(print_parser)

    args = parser.parse_args()
    args.func(args)


main()
