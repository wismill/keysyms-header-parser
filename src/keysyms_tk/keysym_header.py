#!/usr/bin/env python3

from __future__ import annotations

import logging
import os
from itertools import chain
from pathlib import Path
from typing import Any, Generator

from .keysym_entry import KeysymEntry, KeysymEntryParser, KeysymName, KeysymRegistry
from .unicode import NameAliases

################################################################################
# Utils
################################################################################


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


################################################################################
# Keysyms headers
################################################################################

DEFAULT_KEYSYMS_HEADERS_PREFIX = Path("/usr")
DEFAULT_KEYSYMS_HEADERS = (
    Path("include/X11/keysymdef.h"),
    Path("include/X11/XF86keysym.h"),
    Path("include/X11/Sunkeysym.h"),
    Path("include/X11/DECkeysym.h"),
    Path("include/X11/HPkeysym.h"),
)


def get_system_headers() -> tuple[Path, ...]:
    prefix = Path(os.environ.get("X11_HEADERS_PREFIX", DEFAULT_KEYSYMS_HEADERS_PREFIX))
    return tuple(prefix / p for p in DEFAULT_KEYSYMS_HEADERS)


def parse_iter(
    path: Path,
    keysyms: KeysymRegistry | None = None,
    name_aliases: NameAliases | None = None,
) -> Generator[tuple[str, KeysymEntry | None], Any, None]:
    """
    Parse a keysym header file.

    :param path: Path of the header file to parse.
    :param keysyms:
        Mapping of keysym values to keysym names.
        When parsing multiple files, it allows to detect deprecated keysyms across
        the files.

    :returns: A generator that yields the line and a keysym, if available.
    """
    keysymsʹ: KeysymRegistry = keysyms or KeysymRegistry({}, {})
    with path.open("rt", encoding="utf-8") as fd:
        pending_multiline_comment = False
        pending_ifndef: KeysymName | None = None
        for line in fd:
            lineʹ = line.strip()
            if pending_multiline_comment:
                # Continuation of a multiline comment.
                # Check if it ends on this line.
                if lineʹ.endswith("*/"):
                    pending_multiline_comment = False
                yield line, None
            elif lineʹ.startswith("/*"):
                # Start of a multiline comment
                if not lineʹ.endswith("*/"):
                    pending_multiline_comment = True
                yield line, None
            elif pending_ifndefʹ := KeysymEntryParser.parse_guarded_keysym_entry(line):
                # Guard keysym
                if pending_ifndef:
                    raise ValueError(
                        f"Nested #ifndef: current: {pending_ifndef}, new: {pending_ifndefʹ}"
                    )
                else:
                    pending_ifndef = pending_ifndefʹ
                yield line, None
            elif line.startswith("#endif"):
                if pending_ifndef:
                    # Unguard keysym
                    pending_ifndef = None
                yield line, None
            elif (
                result := KeysymEntryParser.parse_keysym_entry(
                    name_aliases, keysymsʹ, pending_ifndef, line
                )
            ) and result.ok:
                # Valid keysym entry
                yield line, result.entry
            else:
                yield line, None


def parse_iter_keysyms(
    path: Path,
    keysyms: KeysymRegistry | None = None,
    name_aliases: NameAliases | None = None,
) -> Generator[KeysymEntry, Any, None]:
    """
    Parse a keysym header file.

    :param path: Path of the header file to parse.
    :param keysyms:
        Mapping of keysym values to keysym names.
        When parsing multiple files, it allows to detect deprecated keysyms across
        the files.

    :returns: A generator that yields the keysyms.
    """
    for _, entry in parse_iter(path, keysyms, name_aliases):
        if entry:
            yield entry


def parse_keysyms(*paths: Path) -> dict[str, KeysymEntry]:
    """
    Parse a keysym header files.

    :param path: Path of the header file to parse.
    :param keysyms:
        Mapping of keysym values to keysym names.
        When parsing multiple files, it allows to detect deprecated keysyms across
        the files.

    :returns: A mapping from keysym name to keysym entry.
    """
    keysymsNames = KeysymRegistry({}, {})
    return {
        entry.name: entry
        for entry in chain(*(parse_iter_keysyms(path, keysymsNames) for path in paths))
    }
