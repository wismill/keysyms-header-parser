"""
`keysyms-tk` is a Python library to parse key symbols (aka _keysyms_)
header files.
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

setup(
    name="keysyms-tk",
    # Versions should comply with PEP 440:
    # https://www.python.org/dev/peps/pep-0440/
    version="0.0.1",
    description="Parser for key symbols (aka keysyms) header files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.freedesktop.org/wismill/keysyms-header-parser",
    author="Pierre Le Marre",
    author_email="dev@wismill.eu",
    license="MIT",
    # For a list of valid classifiers, see https://pypi.org/classifiers/
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: System",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.10",
        "Programming Language :: Python :: 3.11",
        "Programming Language :: Python :: 3 :: Only",
    ],
    keywords="development, keysyms, x11, xkbcommon",
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    python_requires=">=3.10, <4",
    install_requires=[],
    project_urls={
        "Bug Reports": "https://gitlab.freedesktop.org/wismill/keysyms-header-parser/-/issues",
        "Source": "https://gitlab.freedesktop.org/wismill/keysyms-header-parser",
    },
)
